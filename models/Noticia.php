<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "noticia".
 *
 * @property int $idNoticia
 * @property string|null $titular
 * @property string|null $textoCorto
 * @property string|null $textoLargo
 * @property int|null $portada
 * @property int|null $seccion
 * @property string|null $fecha
 * @property string|null $foto
 * @property int|null $autor
 */
class Noticia extends \yii\db\ActiveRecord
{
    public $archivo; // esta es la foto subida
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idNoticia'], 'required'],
            [['idNoticia', 'portada', 'seccion', 'autor'], 'integer'],
            [['textoLargo'], 'string'],
            [['fecha'], 'safe'],
            [['titular', 'foto'], 'string', 'max' => 255],
            [['textoCorto'], 'string', 'max' => 800],
            [['idNoticia'], 'unique'],
            [
                ['archivo'], 'file',
                'skipOnEmpty' => true, // no es obligatorio seleccionas una imagen
                'extensions' => 'png' // extensiones permitidas
            ],
            [['autor'], 'exist', 'skipOnError' => true, 'targetClass' => Autor::class, 'targetAttribute' => ['autor' => 'id']],
            [['seccion'], 'exist', 'skipOnError' => true, 'targetClass' => Seccion::class, 'targetAttribute' => ['seccion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idNoticia' => 'Id Noticia',
            'titular' => 'Titular',
            'textoCorto' => 'Texto Corto',
            'textoLargo' => 'Texto Largo',
            'portada' => 'Portada',
            'seccion' => 'Seccion',
            'fecha' => 'Fecha',
            'foto' => 'Foto',
            'fichero' => 'Seleccione imagen',
            'autor' => 'Autor',
        ];
    }

    public function subirArchivo(): bool
    {
        $this->archivo->saveAs('imgs/' . $this->idNoticia . $this->archivo->name);
        return true;
    }

    /**
     * antes de validar cojo los archivos enviados y
     * los coloco en el modelo
     * 
     */
    public function beforeValidate()
    {
        // si he seleccionado un archivo en el formulario
        if (isset($this->archivo)) {
            $this->archivo = \yii\web\UploadedFile::getInstance($this, 'archivo');
        }

        return true;
    }


    /**
     * despues de validar subo el archivo
     * 
     */
    public function afterValidate()
    {
        // compruebo si he seleccionado un archivo en el formulario
        if (isset($this->archivo)) {
            $this->subirArchivo();
            $this->foto = $this->idNoticia . $this->archivo->name;
        }

        return true;
    }

    /**
     * metodo que se ejecuta despues de guardar el registro 
     * en la bbdd
     * 
     * @param mixed $insert este argumento es true si estas insertando un registro y false si es una actualizacion
     * @param array $atributosAnteriores tengo todos los datos de la tabla antes de actualizar
     * @return void
     */
    public function afterSave($insert, $atributosAnteriores)
    {
        // pregunto si es una actualizacion
        if (!$insert) {
            // pregunto si he seleccionado un archivo en el formulario
            if (isset($this->archivo)) {
                // compruebo si existia foto anteriormente en la noticia
                if (isset($atributosAnteriores["foto"]) && $atributosAnteriores["foto"] != "") {
                    // elimino la imagen vieja del servidor
                    unlink('imgs/' . $atributosAnteriores["foto"]);
                }
            }
        }
    }

    public function getSecciones()
    {
        $secciones = Seccion::find()->all();
        return ArrayHelper::map($secciones, 'id', 'nombre');
    }

    public function getAutores()
    {
        $autores = Autor::find()->all();
        return ArrayHelper::map($autores, 'id', 'nombre');
    }

    public function afterDelete()
    {
        // elimino la imagen de la noticia
        // cuando borro la noticia
        if (isset($this->foto) && file_exists(Yii::getAlias('@webroot') . '/imgs/' . $this->foto)) {
            unlink('imgs/' . $this->foto);
        }
        return true;
    }

    /**
     * Gets query for [[Autor0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAutor0()
    {
        return $this->hasOne(Autor::class, ['id' => 'autor']);
    }
    /**
     * Gets query for [[Seccion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSeccion0()
    {
        return $this->hasOne(Seccion::class, ['id' => 'seccion']);
    }
}
