<?php

namespace app\controllers;

use app\models\Autor;
use app\models\LoginForm;
use app\models\Noticia;
use app\models\Seccion;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['*'],
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Mostrar las noticias de portada
     *
     * @return string
     */
    public function actionIndex()
    {
        // necesito recuperar todas las
        // noticias de la base de datos
        $consulta = Noticia::find(); // consulta sin ejecutar (activeQuery)

        //$datos = $consulta->all();  // array modelos de noticias
        //$datos = $consulta->asArray()->all(); // array de noticias

        // noticias de portada
        $datos = $consulta
            ->where(["portada" => 1]) // seleccionar
            //->where("portada=1") // seleccionar
            ->all(); // ejecutar

        return $this->render('index', [
            "datos" => $datos,
            "titulo" => "Noticias de Portada"
        ]);
    }
    public function actionViewnoticia($idNoticia)
    {
        $consulta = Noticia::find()->where([
            "idNoticia" => $idNoticia
        ]); // select * from noticia where idNoticia=1

        $dato = $consulta->one();

        return $this->render("verNoticia", [
            "dato" => $dato
        ]);
    }

    public function actionSecciones()
    {
        $secciones = Seccion::find()->all(); // select * from seccion

        // le mando a la vista secciones
        // todas las secciones de la base de datos
        return $this->render('secciones', [
            "secciones" => $secciones
        ]);

        //return $this->render('secciones', compact("secciones"));
    }

    public function actionSeccion($id)
    {
        // listar las noticias cuya seccion 
        // sea el id mandado por la URL
        $noticias = Noticia::find()
            ->where(["seccion" => $id])
            ->all();

        // saco todos los datos de la seccion seleccionada
        $seccion = Seccion::find()->where(["id" => $id])->one();
        //$seccion = Seccion::findOne($id);

        // aprovecho la vista index para mostrar las noticias 
        // de una seccion determinada
        return $this->render('index', [
            "datos" => $noticias,
            "titulo" => "Noticias de la Seccion " . $seccion->nombre
        ]);
    }

    public function actionAutores()
    {
        // obtengo todos los autores
        $autores = Autor::find()->all();

        // le mando a la vista todos los autores
        return $this->render("autores", [
            "autores" => $autores
        ]);
    }

    public function actionAutor($id)
    {
        // recuperar las noticias de ese autor (el que le mando por la URL)
        $noticias = Noticia::find()->where(["autor" => $id])->all();

        // obtener todos los datos del autor seleccionado
        $autor = Autor::find()->where(["id" => $id])->one();

        // aprovecho la vista index para mostrar las noticias 
        // de una seccion determinada
        return $this->render('index', [
            "datos" => $noticias,
            "titulo" => "Noticias de " . $autor->nombre
        ]);
    }
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
