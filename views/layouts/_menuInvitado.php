<?php

use yii\bootstrap5\Nav;
use yii\helpers\Html;


echo Nav::widget([
    'options' => ['class' => 'navbar-nav'],
    'items' => [
        ['label' => 'Home', 'url' => ['/site/index']],
        ['label' => 'Secciones', 'url' => ['/site/secciones']],
        ['label' => 'Autores', 'url' => ['/site/autores']],
        ['label' => 'Login', 'url' => ['/site/login']],
    ]
]);
