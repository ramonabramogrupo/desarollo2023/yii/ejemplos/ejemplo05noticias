<?php

use yii\bootstrap5\Nav;
use yii\helpers\Html;


echo Nav::widget([
    'options' => ['class' => 'navbar-nav'],
    'items' => [
        ['label' => 'Home', 'url' => ['/site/index']],
        ['label' => 'Secciones', 'url' => ['/site/secciones']],
        ['label' => 'Autores', 'url' => ['/site/autores']],
        ['label' => 'Administracion', 'items' => [
            ['label' => 'Noticias', 'url' => ['/noticia/index']],
            ['label' => 'Secciones', 'url' => ['/seccion/index']],
            ['label' => 'Autores', 'url' => ['/autor/index']],
        ]],
        '<li class="nav-item">'
            . Html::beginForm(['/site/logout'])
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'nav-link btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
    ]
]);
