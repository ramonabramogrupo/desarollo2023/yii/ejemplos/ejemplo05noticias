<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Noticia $model */

$this->title = $model->idNoticia;
$this->params['breadcrumbs'][] = ['label' => 'Noticias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="noticia-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'idNoticia' => $model->idNoticia], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'idNoticia' => $model->idNoticia], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas seguro de eliminar la noticia?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idNoticia',
            'titular',
            'textoCorto',
            'textoLargo:ntext',
            //'portada',
            // 'seccion' // saca el id de la seccion
            //'seccion0.nombre', // sacar el nombre de la seccion
            [
                'attribute' => 'seccion',
                'value' => function ($model) {
                    return $model->seccion0->nombre;
                }
            ],
            // sacar el nombre de la seccion
            'fecha',
            //'foto',
            // 'autor' // saca el id del autor
            //'autor0.nombre', // SACAR EL CAMPO NOMBRE DEL AUTOR
            [
                'attribute' => 'autor',
                'value' => function ($model) {
                    return $model->autor0->nombre;
                }
            ], // sacar el nombre del autor
            // forma de colocar una imagen en DETAILVIEW
            [
                'attribute' => 'foto',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img('@web/imgs/' . $model->foto, ['class' => 'col-lg-4']);
                }
            ],
            // modificar la presentacion del campo portada
            [
                'attribute' => 'portada',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->portada == 1 ? '<i class="fas fa-check-square"></i>' : '<i class="fas fa-times-square"></i>';
                }
            ],
        ],
    ]) ?>

</div>