<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Autor $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="autor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->input('number', [
        'readonly' => 'readonly'
    ]) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'archivo')->fileInput() ?>

    <?= Html::img('@web/imgs/autores/' . $model->foto, [
        "class" => 'col-lg-2'
    ]); ?>

    <?= $form->field($model, 'fechaNacimiento')->input('date') ?>

    <?= $form->field($model, 'correo')->input('email') ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>